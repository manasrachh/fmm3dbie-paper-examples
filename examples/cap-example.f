      implicit real *8 (a-h,o-z) 
      real *8, allocatable :: srcvals(:,:),srccoefs(:,:)
      real *8, allocatable :: wts(:)
      character *100 fname,title
      integer ipars(2)

      integer, allocatable :: norders(:),ixyzs(:),iptype(:)

      real *8 xyz_out(3),xyz_in(3)
      real *8, allocatable :: sigma(:),rhs(:)
      real *8, allocatable :: errs(:)
      real *8 eps_gmres
      real *8 dpars(2)
      real *8, allocatable :: vs(:),qs(:)
      integer, allocatable :: icomp_pstart(:)

      complex *16 zpars
      integer numit,niter

      integer ipatch_id
      real *8 uvs_targ(2)

      logical isout0,isout1

      real *8 pot,potex
      complex *16 ztmp,ima

      data ima/(0.0d0,1.0d0)/


      call prini(6,13)

      done = 1
      pi = atan(done)*4

      iref = 2
      write(fname,'(a,i1,a)') 
     1   '../../fmm3dbie/geometries/Capacitor_3_o03_r0',iref,'.go3'

      
      call open_gov3_geometry_mem(fname,npatches0,npts0)

      npatches = 2*npatches0
      npts = 2*npts0

      call prinf('npatches=*',npatches,1)
      call prinf('npts=*',npts,1)


      allocate(srcvals(12,npts),srccoefs(9,npts))
      allocate(ixyzs(npatches+1),iptype(npatches),norders(npatches))
      allocate(wts(npts))

      call open_gov3_capacitor1(fname,npts0,norder,npatches0,
     1   srcvals,srccoefs,wts)

      npols = (norder+1)*(norder+2)/2
      do i=1,npatches
        norders(i) = norder
        iptype(i) = 1
        ixyzs(i) = (i-1)*npols+1
      enddo
      ixyzs(npatches+1) = npts+1

      call prinf('npatches0=*',npatches0,1)
      call prinf('npts0=*',npts0,1)

      call surf_quadratic_msh_vtk_plot(npatches,norders,ixyzs,iptype,
     1  npts,srccoefs,srcvals,'cap-mesh.vtk','cap-mesh')


      ncomp = 2

      
      
      allocate(sigma(npts),vs(ncomp),qs(ncomp),icomp_pstart(ncomp+1))

      icomp_pstart(1) = 1
      icomp_pstart(2) = 1+npatches/2
      icomp_pstart(3) = npatches+1

      qs(1) = -1.0d0
      qs(2) = 1.0d0

      call prinf('icomp_pstart=*',icomp_pstart,3)

      do i=1,npts
        sigma(i) = 0 
      enddo
      vs(1) = 0
      vs(2) = 0



      numit = 200
      ifinout = 0
      niter = 0
      allocate(errs(numit+1))

      eps = 0.51d-6

      eps_gmres = eps

      call cpu_time(t1)
C$      t1 = omp_get_wtime()
      call lap_comb_elas_solver(npatches,norders,ixyzs,iptype,npts,
     1  srccoefs,srcvals,ncomp,icomp_pstart,eps,numit,qs,eps_gmres,
     2  niter,errs,rres,sigma,vs)
      call cpu_time(t2)
C$      t2 = omp_get_wtime()

      call prinf('niter=*',niter,1)
      call prin2('rres=*',rres,1)
      call prin2('errs=*',errs,niter)

      call prin2('solve time=*',t2-t1,1)

      call prin2('vs=*',vs,2)

      cap = 1.0d0/(vs(2)-vs(1))

      call prin2('capacitance=*',cap,1)
      write(fname,'(a,i1,a)') 'cap-res/cap_dens_o03_r0',iref,'.vtk'
      title = 'cap dens - real part'
      call surf_vtk_plot_scalar(npatches,norders,ixyzs,iptype,npts,
     1   srccoefs,srcvals,sigma,fname,title)
      
      open(unit=33,file='cap-res/res-sum.txt',access='append')
      write(33,'(2x,i1,2x,i1,2x,e11.5)') iref,norder,cap 
      
      




      stop
      end




      subroutine setup_geom(igeomtype,norder,npatches,ipars, 
     1    srcvals,srccoefs,ifplot,fname)
      implicit real *8 (a-h,o-z)
      integer igeomtype,norder,npatches,ipars(*),ifplot
      character (len=*) fname
      real *8 srcvals(12,*), srccoefs(9,*)
      real *8, allocatable :: uvs(:,:),umatr(:,:),vmatr(:,:),wts(:)

      real *8, pointer :: ptr1,ptr2,ptr3,ptr4
      integer, pointer :: iptr1,iptr2,iptr3,iptr4
      real *8, target :: p1(10),p2(10),p3(10),p4(10)
      real *8, allocatable, target :: triaskel(:,:,:)
      real *8, allocatable, target :: deltas(:,:)
      integer, allocatable :: isides(:)
      integer, target :: nmax,mmax

      procedure (), pointer :: xtri_geometry


      external xtri_stell_eval,xtri_sphere_eval
      
      npols = (norder+1)*(norder+2)/2
      allocate(uvs(2,npols),umatr(npols,npols),vmatr(npols,npols))
      allocate(wts(npols))

      call vioreanu_simplex_quad(norder,npols,uvs,umatr,vmatr,wts)

      if(igeomtype.eq.1) then
        itype = 2
        allocate(triaskel(3,3,npatches))
        allocate(isides(npatches))
        npmax = npatches
        ntri = 0
        call xtri_platonic(itype, ipars(1), npmax, ntri, 
     1      triaskel, isides)

        xtri_geometry => xtri_sphere_eval
        ptr1 => triaskel(1,1,1)
        ptr2 => p2(1)
        ptr3 => p3(1)
        ptr4 => p4(1)


        if(ifplot.eq.1) then
           call xtri_vtk_surf(fname,npatches,xtri_geometry, ptr1,ptr2, 
     1         ptr3,ptr4, norder,'Triangulated surface of the sphere')
        endif


        call getgeominfo(npatches,xtri_geometry,ptr1,ptr2,ptr3,ptr4,
     1     npols,uvs,umatr,srcvals,srccoefs)
      endif

      if(igeomtype.eq.2) then
        done = 1
        pi = atan(done)*4
        umin = 0
        umax = 2*pi
        vmin = 2*pi
        vmax = 0 
        allocate(triaskel(3,3,npatches))
        nover = 0
        call xtri_rectmesh_ani(umin,umax,vmin,vmax,ipars(1),ipars(2),
     1     nover,npatches,npatches,triaskel)

        mmax = 2
        nmax = 1
        xtri_geometry => xtri_stell_eval

        allocate(deltas(-1:mmax,-1:nmax))
        deltas(-1,-1) = 0.17d0
        deltas(0,-1) = 0
        deltas(1,-1) = 0
        deltas(2,-1) = 0

        deltas(-1,0) = 0.11d0
        deltas(0,0) = 1
        deltas(1,0) = 4.5d0
        deltas(2,0) = -0.25d0

        deltas(-1,1) = 0
        deltas(0,1) = 0.07d0
        deltas(1,1) = 0
        deltas(2,1) = -0.45d0

        ptr1 => triaskel(1,1,1)
        ptr2 => deltas(-1,-1)
        iptr3 => mmax
        iptr4 => nmax

        if(ifplot.eq.1) then
           call xtri_vtk_surf(fname,npatches,xtri_geometry, ptr1,ptr2, 
     1         iptr3,iptr4, norder,
     2         'Triangulated surface of the stellarator')
        endif

        call getgeominfo(npatches,xtri_geometry,ptr1,ptr2,iptr3,iptr4,
     1     npols,uvs,umatr,srcvals,srccoefs)
      endif
      
      return  
      end


      subroutine test_exterior_pt(npatches,norder,npts,srcvals,
     1   srccoefs,wts,xyzout,isout)
c
c
c  this subroutine tests whether the pt xyzin, is
c  in the exterior of a surface, and also estimates the error
c  in representing e^{ir/2}/r and \grad e^{ir/2}/r \cdot n
c  centered at the interior point. Whether a point 
c  is in the interior or not is tested using Gauss' 
c  identity for the flux due to a point charge
c
c
c  input:
c    npatches - integer
c       number of patches
c    norder - integer
c       order of discretization
c    npts - integer
c       total number of discretization points on the surface
c    srccoefs - real *8 (9,npts)
c       koornwinder expansion coefficients of geometry info
c    xyzout -  real *8 (3)
c       point to be tested
c
c  output: 
c    isout - boolean
c      whether the target is in the interior or not
c

      implicit none
      integer npatches,norder,npts,npols
      real *8 srccoefs(9,npts),srcvals(12,npts),xyzout(3),wts(npts)
      real *8 tmp(3)
      real *8 dpars,done,pi
      real *8, allocatable :: rsurf(:),err_p(:,:) 
      integer ipars,norderhead,nd
      complex *16, allocatable :: sigma_coefs(:,:), sigma_vals(:,:)
      complex *16 zk,val

      integer ipatch,j,i
      real *8 ra,ds
      logical isout

      done = 1
      pi = atan(done)*4

      npols = (norder+1)*(norder+2)/2


      zk = 0

      ra = 0



      do ipatch=1,npatches
        do j=1,npols
          i = (ipatch-1)*npols + j
          call h3d_sprime(xyzout,12,srcvals(1,i),0,dpars,1,zk,0,ipars,
     1       val)

          call cross_prod3d(srcvals(4,i),srcvals(7,i),tmp)
          ds = sqrt(tmp(1)**2 + tmp(2)**2 + tmp(3)**2)
          ra = ra + real(val)*wts(i)
        enddo
      enddo

      if(abs(ra+4*pi).le.1.0d-1) isout = .false.
      if(abs(ra).le.1.0d-1) isout = .true.

      return
      end

   







      subroutine open_gov3_capacitor1(filename,npoints,norder,ntri,
     1   srcvals,srccoefs,wts)
      implicit none

c   List of calling arguments
      character (len=100), intent(in) :: filename
	  integer ( kind = 4 ), intent(in) :: npoints
	  integer ( kind = 4 ), intent(out) :: norder,ntri
	  real ( kind = 8 ), intent(out) :: srcvals(12,2*npoints)
	  real ( kind = 8 ), intent(out) :: srccoefs(9,2*npoints)
	  real ( kind = 8 ), intent(out) :: wts(2*npoints)
	
c  List of local variables
      integer ( kind = 4 ) umio,count1,count2,flag,aux,npols,icount
      integer :: ierror
	  real ( kind = 8 ) aux_real,aux_vect(3)
	  real ( kind = 8 ), allocatable :: h_points(:),h_coefs(:)
      real ( kind = 8 ), allocatable :: uv(:,:),umatr(:,:),
     1    vmatr(:,:),w(:)

      open(UNIT=8, FILE=filename, STATUS='OLD', ACTION='READ', 
     1   IOSTAT=ierror)

      read(8,*) norder
      read(8,*) ntri

      do count1=1,npoints
        read(8,*) srcvals(1,count1) 
      enddo
      do count1=1,npoints
        read(8,*) srcvals(2,count1) 
      enddo
      do count1=1,npoints
        read(8,*) srcvals(3,count1) 
      enddo

      do count1=1,npoints
        read(8,*) srcvals(4,count1) 
      enddo

      do count1=1,npoints
        read(8,*) srcvals(5,count1) 
      enddo
      do count1=1,npoints
        read(8,*) srcvals(6,count1) 
      enddo


      do count1=1,npoints
        read(8,*) srcvals(7,count1) 
      enddo
      do count1=1,npoints
        read(8,*) srcvals(8,count1)
      enddo
      do count1=1,npoints
        read(8,*) srcvals(9,count1) 
      enddo

      do count1=1,npoints
        read(8,*) srcvals(10,count1) 
      enddo
      do count1=1,npoints
        read(8,*) srcvals(11,count1)
      enddo
      do count1=1,npoints
        read(8,*) srcvals(12,count1) 
      enddo

      close (8)
	
      npols = (norder+1)*(norder+2)/2
      allocate(h_points(npols))
      allocate(h_coefs(npols))
      allocate(uv(2,npols),umatr(npols,npols),vmatr(npols,npols),
     1   w(npols))
      call vioreanu_simplex_quad(norder,npols,uv,umatr,vmatr,w)


      icount=1
      do count1=1,ntri
        do count2=1,npols
          call cross_prod3d(srcvals(4:6,icount),
     1       srcvals(7:9,icount),aux_vect)
          aux_real=sqrt(aux_vect(1)**2+aux_vect(2)**2+aux_vect(3)**2)
          wts(icount)=w(count2)*aux_real
          icount=icount+1
        enddo		
      enddo
		
      do count1=1,npoints
        srcvals(1,count1+npoints)=1.0d0-srcvals(1,count1)
        srcvals(2,count1+npoints)=-15.0d0-srcvals(2,count1)
        srcvals(3,count1+npoints)=srcvals(3,count1)
			
        srcvals(4,count1+npoints)=-srcvals(4,count1)
        srcvals(5,count1+npoints)=-srcvals(5,count1)
        srcvals(6,count1+npoints)=srcvals(6,count1)
			
        srcvals(7,count1+npoints)=-srcvals(7,count1)
        srcvals(8,count1+npoints)=-srcvals(8,count1)
        srcvals(9,count1+npoints)=srcvals(9,count1)
		
        srcvals(10,count1+npoints)=-srcvals(10,count1)
        srcvals(11,count1+npoints)=-srcvals(11,count1)
        srcvals(12,count1+npoints)=srcvals(12,count1)
		wts(count1+npoints)=wts(count1)
      enddo
		

      do count1=1,ntri*2
        h_points(:)=srcvals(1,(count1-1)*npols+1:count1*npols)
        call dmatvec(npols,npols,umatr,h_points,h_coefs)		
        srccoefs(1,(count1-1)*npols+1:count1*npols)=h_coefs(:)					

        h_points(:)=srcvals(2,(count1-1)*npols+1:count1*npols)
        call dmatvec(npols,npols,umatr,h_points,h_coefs)		
        srccoefs(2,(count1-1)*npols+1:count1*npols)=h_coefs(:)		

        h_points(:)=srcvals(3,(count1-1)*npols+1:count1*npols)
        call dmatvec(npols,npols,umatr,h_points,h_coefs)		
        srccoefs(3,(count1-1)*npols+1:count1*npols)=h_coefs(:)		

        h_points(:)=srcvals(4,(count1-1)*npols+1:count1*npols)
        call dmatvec(npols,npols,umatr,h_points,h_coefs)		
        srccoefs(4,(count1-1)*npols+1:count1*npols)=h_coefs(:)					
			
        h_points(:)=srcvals(5,(count1-1)*npols+1:count1*npols)
        call dmatvec(npols,npols,umatr,h_points,h_coefs)		
        srccoefs(5,(count1-1)*npols+1:count1*npols)=h_coefs(:)		

        h_points(:)=srcvals(6,(count1-1)*npols+1:count1*npols)
        call dmatvec(npols,npols,umatr,h_points,h_coefs)		
        srccoefs(6,(count1-1)*npols+1:count1*npols)=h_coefs(:)		

        h_points(:)=srcvals(7,(count1-1)*npols+1:count1*npols)
        call dmatvec(npols,npols,umatr,h_points,h_coefs)		
        srccoefs(7,(count1-1)*npols+1:count1*npols)=h_coefs(:)					
			
        h_points(:)=srcvals(8,(count1-1)*npols+1:count1*npols)
        call dmatvec(npols,npols,umatr,h_points,h_coefs)		
        srccoefs(8,(count1-1)*npols+1:count1*npols)=h_coefs(:)		

        h_points(:)=srcvals(9,(count1-1)*npols+1:count1*npols)
        call dmatvec(npols,npols,umatr,h_points,h_coefs)		
        srccoefs(9,(count1-1)*npols+1:count1*npols)=h_coefs(:)		

      enddo

      return
      end
