      implicit real *8 (a-h,o-z) 
      real *8, allocatable :: srcvals(:,:),srccoefs(:,:)
      real *8, allocatable :: wts(:),rsigma(:)
      integer ipars(2)

      integer, allocatable :: norders(:),ixyzs(:),iptype(:)
      integer, allocatable :: row_ptr(:),col_ind(:)
      real *8, allocatable :: xyz_in(:,:),xyz_out(:,:)
      real *8, allocatable :: xyz_src(:,:)
      real *8, allocatable :: rads(:),cms(:,:),rad_near(:)
      real *8, allocatable :: targs(:,:)

      complex *16, allocatable :: sigma(:),rhs(:),potval(:)
      complex *16, allocatable :: sigma2(:),rhs2(:),charges(:)
      real *8, allocatable :: errs(:)
      real *8 thet,phi
      complex * 16 zpars(3)
      integer numit,niter
      character *100 title,fname

      integer, allocatable :: ipatch_id(:)
      real *8, allocatable :: uvs_targ(:,:)

      logical isout0,isout1
      logical, allocatable :: isout(:)

      complex *16 pot,potex,ztmp,ima

      data ima/(0.0d0,1.0d0)/


      call prini(6,13)

      done = 1
      pi = atan(done)*4

c
c   simulation for plane 50 wavelengths in size
c
      zk = 15.7d0*2
      zpars(1) = zk 
      zpars(2) = -ima*zk
      zpars(3) = 2.0d0

      

      fname = '../../fmm3dbie/geometries/A380_Final_o03_r02.go3'
      
      call open_gov3_geometry_mem(fname,npatches,npts)

      call prinf('npatches=*',npatches,1)
      call prinf('npts=*',npts,1)

      allocate(srcvals(12,npts),srccoefs(9,npts))
      allocate(ixyzs(npatches+1),iptype(npatches),norders(npatches))
      allocate(wts(npts))

      call open_gov3_geometry(fname,npatches,norders,ixyzs,
     1   iptype,npts,srcvals,srccoefs,wts)
      
cc      fname = 'plane-res/a380.vtk'
cc      title = 'a380'
cc      call surf_vtk_plot(npatches,norders,ixyzs,iptype,npts,
cc     1   srccoefs,srcvals,fname,title)

      norder = norders(1)

      ntot = 100000
      allocate(xyz_in(3,ntot))
      allocate(isout(ntot))

      nsrc = 1000
      do i=1,nsrc
        xyz_in(1,i) = hkrand(0)*6.5 + 1.0
        xyz_in(2,i) = hkrand(0) - 0.5d0
        xyz_in(3,i) = hkrand(0)*0.75d0
      enddo

      nlatx = 30
      nlaty = 10
      nlatz = 30

      nlatx = 0
      nlaty = 0
      nlatz = 0

      ntot = nsrc + nlatx*nlaty*nlatz
      
      xmin = 7.5d0
      xmax = 11.0d0
      ymin = -0.15d0
      ymax = 0.15d0
      zmin = 0.75d0
      zmax = 2.25d0

      i = nsrc
      do ix=1,nlatx
        do iy=1,nlaty
          do iz =1,nlatz
            i = i+1
            xyz_in(1,i) = xmin + (ix-1.0d0)*(xmax-xmin)/(nlatx-1)
            xyz_in(2,i) = ymin + (iy-1.0d0)*(ymax-ymin)/(nlaty-1)
            xyz_in(3,i) = zmin + (iz-1.0d0)*(zmax-zmin)/(nlatz-1)
          enddo
        enddo
      enddo

      do i=1,ntot
        call test_exterior_pt(npatches,norders,npts,srcvals,srccoefs,
     1    wts,xyz_in(1,i),isout(i))
      enddo
      allocate(cms(3,npatches),rads(npatches),rad_near(npatches))
      call get_centroid_rads(npatches,norders,ixyzs,iptype,npts, 
     1     srccoefs,cms,rads)
      do i=1,npatches
        rad_near(i) = rads(i)*8.0d0
      enddo
      
      call findnearmem(cms,npatches,rad_near,3,xyz_in,ntot,nnz)
      call prinf('nnz=*',nnz,1)
      allocate(row_ptr(ntot+1),col_ind(nnz))
      call findnear(cms,npatches,rad_near,3,xyz_in,ntot,row_ptr, 
     1        col_ind)

      nct = 0
      do i=1,nsrc
        nnear = row_ptr(i+1)-row_ptr(i)
        print *, i,nnear,isout(i),xyz_in(2,i)
        if((.not.isout(i)).and.nnear.eq.0) then
          nct = nct + 1
        endif
      enddo
      nfuse = nct
      open(unit=73,file='fuselage-sources.txt')
      write(73,*) nfuse
      do i=1,nsrc
        nnear = row_ptr(i+1)-row_ptr(i)
        if((.not.isout(i)).and.nnear.eq.0) then
          write(73,*) xyz_in(1,i),xyz_in(2,i),xyz_in(3,i)
        endif
      enddo
      close(73)
      call prinf('sources found in fuselage=*',nct,1)

      nct = 0
      do i=nsrc+1,ntot
        nnear = row_ptr(i+1)-row_ptr(i)
        print *, i,nnear,isout(i),xyz_in(2,i)
        if((.not.isout(i)).and.nnear.eq.0) then
          nct = nct + 1
        endif
      enddo
      ntail = nct

      open(unit=73,file='tail-sources.txt')
      write(73,*) ntail
      do i=nsrc+1,ntot
        nnear = row_ptr(i+1)-row_ptr(i)
        if((.not.isout(i)).and.nnear.eq.0) then
          write(73,*) xyz_in(1,i),xyz_in(2,i),xyz_in(3,i)
        endif
      enddo
      close(73)

      call prinf('sources found in tail=*',nct,1)

      stop

      nlat = 301
      ntarg = nlat*nlat

      allocate(targs(3,ntarg),ipatch_id(ntarg),uvs_targ(2,ntarg))

      do i=1,nlat
        do j=1,nlat
          ipt = (i-1)*nlat + j
          targs(1,ipt) = (j-1.0d0)/(nlat-1.0d0)*12.0d0 - 0.5d0
          targs(2,ipt) = (i-1.0d0)/(nlat-1.0d0)*12.0d0 - 6.0d0
          targs(3,ipt) = 0
          ipatch_id(ipt) = -1
          uvs_targ(1,ipt) = 0.0d0
          uvs_targ(2,ipt) = 0.0d0
        enddo
      enddo


      zpars(1) = 1.0d-6 
      zpars(2) = 0.0d0 
      zpars(3) = 1.0d0

      allocate(sigma(npts),potval(ntarg))
      do i=1,npts
        sigma(i) = 1.0d0
      enddo

      eps = 1.0d-6

      goto 1111

      call lpcomp_helm_comb_dir(npatches,norders,ixyzs,iptype,npts,
     1  srccoefs,srcvals,3,ntarg,targs,ipatch_id,uvs_targ,eps,zpars,
     2  sigma,potval)
      
      do i=1,ntarg
        write(78,'(e11.5)') real(potval(i))
        write(79,*) targs(1,i),targs(2,i),targs(3,i),real(potval(i)),
     1     imag(potval(i))
      enddo

 1111 continue      

      ntot = 0
      open(unit=39,file='tail-sources.txt')
      read(39,*) ntail
      open(unit=40,file='fuselage-sources.txt')
      read(40,*) nfuse

      ntot = nfuse + ntail
      allocate(xyz_src(3,ntot))
      do i=1,ntail
        read(39,*) xyz_src(1,i),xyz_src(2,i),xyz_src(3,i)
      enddo
      do i=1,nfuse
        ii = ntail+i
        read(40,*) xyz_src(1,ii),xyz_src(2,ii),xyz_src(3,ii)
      enddo
      close(39)
      close(40)

      call prin2('xyz_src=*',xyz_src,3*ntot)

      allocate(charges(ntot))

      open(unit=40,file='charges.txt') 
      do i=1,ntot
        charges(i) = hkrand(0) - 0.5d0
        write(40,*) real(charges(i))
      enddo


      allocate(rsigma(npts))
      do i=1,npts
        rsigma(i) = 0
        do j=1,ntot
          call h3d_slp(xyz_src(1,j),3,srcvals(1,i),0,dpars,1,zpars,0,
     1       ipars,pot)
          rsigma(i) = rsigma(i) + real(pot*charges(j))
        enddo
      enddo

      fname = 'plane-res/a380_rsigma_testdens.vtk'
      title = 'a380 - real part'
      call surf_vtk_plot_scalar(npatches,norders,ixyzs,iptype,npts,
     1   srccoefs,srcvals,rsigma,fname,title)

      stop
      end
c
c
c
c
c

      subroutine test_exterior_pt(npatches,norder,npts,srcvals,
     1   srccoefs,wts,xyzout,isout)
c
c
c  this subroutine tests whether the pt xyzin, is
c  in the exterior of a surface, and also estimates the error
c  in representing e^{ir/2}/r and \grad e^{ir/2}/r \cdot n
c  centered at the interior point. Whether a point 
c  is in the interior or not is tested using Gauss' 
c  identity for the flux due to a point charge
c
c
c  input:
c    npatches - integer
c       number of patches
c    norder - integer
c       order of discretization
c    npts - integer
c       total number of discretization points on the surface
c    srccoefs - real *8 (9,npts)
c       koornwinder expansion coefficients of geometry info
c    xyzout -  real *8 (3)
c       point to be tested
c
c  output: 
c    isout - boolean
c      whether the target is in the interior or not
c

      implicit none
      integer npatches,norder,npts,npols
      real *8 srccoefs(9,npts),srcvals(12,npts),xyzout(3),wts(npts)
      real *8 tmp(3)
      real *8 dpars,done,pi
      real *8, allocatable :: rsurf(:),err_p(:,:) 
      integer ipars,norderhead,nd
      complex *16, allocatable :: sigma_coefs(:,:), sigma_vals(:,:)
      complex *16 zk,val

      integer ipatch,j,i
      real *8 ra,ds
      logical isout

      done = 1
      pi = atan(done)*4

      npols = (norder+1)*(norder+2)/2


      zk = 0

      ra = 0



      do ipatch=1,npatches
        do j=1,npols
          i = (ipatch-1)*npols + j
          call h3d_sprime(xyzout,12,srcvals(1,i),0,dpars,1,zk,0,ipars,
     1       val)
          call cross_prod3d(srcvals(4,i),srcvals(7,i),tmp)
          ds = sqrt(tmp(1)**2 + tmp(2)**2 + tmp(3)**2)
          ra = ra + real(val)*wts(i)
        enddo
      enddo

      if(abs(ra+4*pi).le.1.0d-5) isout = .false.
      if(abs(ra).le.1.0d-5) isout = .true.

      return
      end

   




