      implicit real *8 (a-h,o-z) 
      real *8, allocatable :: srcvals(:,:),srccoefs(:,:)
      real *8, allocatable :: wts(:),rsigma(:)
      integer ipars(2)

      integer, allocatable :: norders(:),ixyzs(:),iptype(:)

      real *8 xyz_out(3),xyz_in(3)
      complex *16, allocatable :: sigma(:),rhs(:)
      complex *16, allocatable :: sigma2(:),rhs2(:)

      real *8, allocatable :: errs(:)
      real *8 thet,phi
	  complex * 16  zpars(5), omega, ep0,mu0,ep1,mu1,zk0,zk1
      complex *16 zpars2(4)

      integer numit,niter
      integer ivals(10),ndims(3)
      character *200 title,fname,fname1,fname2
      character *8 date
      character *10 time
      character *5 zone

      real *8 dxyz(3),xyz_start(3)
      real *8, allocatable :: targs(:,:)
      complex *16, allocatable :: pvals(:)
      real *8, allocatable :: evals(:)
      integer, allocatable :: ipatch_id(:)
      real *8, allocatable :: uvs_targ(:,:)

      logical isout0,isout1

      complex *16 pot,potex,ztmp,ima,zk
      complex *16 alpha_rhs

      integer count1

      integer m_fresnel
      complex *16 omega_0

      data ima/(0.0d0,1.0d0)/


      call prini(6,13)

      done = 1
      pi = atan(done)*4

!
! Here we define the helmholtz parameters:    
!
  
    omega=0.330d0
    ep0=1.00000d0
    mu0=1.0d0
    ep1=(2.0d0,0.0d0)
    mu1=1.000d0

! This is for the fresnel simulations:
      m_fresnel=1

      omega_0=1.0d0/(sqrt(ep1*mu1/(ep0*mu0))-1.0d0)
      omega=omega_0*m_fresnel    ! Esto debe ser activado para Fresnel

      write (*,*) 'omega_0: ',omega_0
      write (*,*) 'omega: ', omega
      write (*,*) 'l0: ',36.0d0*omega*sqrt(ep0*mu0)
      write (*,*) 'lg:',36.0d0*omega*sqrt(ep1*mu1)
      write (*,*) 'press to continue...'
      !read (*,*) 

    zpars(1) = omega
    zpars(2) = ep0
    zpars(3) = mu0
    zpars(4) = ep1
    zpars(5) = mu1
    zk0 = omega*sqrt(ep0*mu0)
    zk1 = omega*sqrt(ep1*mu1)
    call prin2('zk0=*',zk0,2)
    call prin2('zk1=*',zk1,2)
  
!	write (*,*) 'Electrical length for the cube (simplest_cube_quadratic): ', 3.0d0*omega*sqrt(ep1*mu1)/(2*3.1415927d0)


      xyz_out(1) = 50.0d0
      xyz_out(2) = 60.0d0
      xyz_out(3) = 70.0d0

!      fname = '../../../../Geometries_go3/' // &
!    & 'simplest_cube_quadratic_v4_o04_r01.go3'
!	  fname2='../../../../Maxwell_FMM_3dquad/Solutions_Maxwell/t_simplest_cube_quadratic_o04_r01.sol'

!      xyz_in(1) = 0.0d0
!      xyz_in(2) = 0.0d0
!      xyz_in(3) = 1.5d0

!      fname = '../../../../Geometries_go3/' // &
!     & 'fresnel_lens_o04_r01.go3'

!omega=0.330d0
!ep1=1.2d0
      fname = '../../geometries/' // &
     & 'fresnel_lens_large_o04_r01.go3'
!	  fname2='../../../../Maxwell_FMM_3dquad/Solutions_Maxwell/fresnel_lens_large_o04_r01.sol'
     fname2='fresnel_lens_large_o04_r01.sol'

      xyz_in(1)=0.0d0
      xyz_in(2)=0.0d0
      xyz_in(3)=0.3d0

!       fname =  '../../../../Geometries_go3/' // &
!      & 'Round_2_o04_r01.go3'
!      xyz_in(1) = 0.00d0
!      xyz_in(2) = 0.0d0
!      xyz_in(3) = 0.0d0

      write (*,*) 'Nombre fichero:', fname
      call open_gov3_geometry_mem(fname,npatches,npts)

      call prinf('npatches=*',npatches,1)
      call prinf('npts=*',npts,1)

      allocate(srcvals(12,npts),srccoefs(9,npts))
      allocate(ixyzs(npatches+1),iptype(npatches),norders(npatches))
      allocate(wts(npts))

      call open_gov3_geometry(fname,npatches,norders,ixyzs,&
     &iptype,npts,srcvals,srccoefs,wts )


!      fname = 'plane-res/a380.vtk'
!      title = 'a380'
!      call surf_vtk_plot(npatches,norders,ixyzs,iptype,npts,
!     1   srccoefs,srcvals,fname,title)

      norder = norders(1)
!      call test_exterior_pt(npatches,norder,npts,srcvals,srccoefs,&
!     &wts,xyz_in,isout0)

      allocate(sigma(2*npts),rhs(2*npts))
      thet = hkrand(0)*pi
      phi = hkrand(0)*2*pi
!     Get right hand side (f and g)

!$OMP PARALLEL DO DEFAULT(SHARED)
      do i=1,npts
         rhs(i) = -exp(ima*zk0*srcvals(3,i))
         rhs(npts+i) = -ima*zk0/ep0*rhs(i)*srcvals(12,i)
      enddo
!$OMP END PARALLEL DO      


!     Start solver (GMRES)

      numit = 400
      niter = 0
      allocate(errs(numit+1))

      eps = 0.51d-6
	  eps_gmres=0.51d-6

!      eps = 1d-3
!	  eps_gmres=1d-4

      call cpu_time(t1)
!$      t1 = omp_get_wtime()     
      write (*,*) 'opening sol'
!      call open_sol(fname2,sigma,2*npts)

      call helm_comb_trans_solver(npatches,norders,ixyzs,iptype,npts, &
       srccoefs,srcvals,eps,zpars,numit,rhs,eps_gmres,niter,errs,rres, &
       sigma)
!      call h_transmission_solver(npatches,norders,ixyzs,iptype,npts,&
!     &srccoefs,srcvals,eps,zpars,numit,ifinout,rhs,eps_gmres,niter,errs,&
!     &rres,sigma)

      call prinf('niter=*',niter,1)
      call prin2('rres=*',rres,1)
      call prin2('errs=*',errs,niter)

      call cpu_time(t2)
!$       t2 = omp_get_wtime()
      call prin2('analytic solve time=*',t2-t1,1)

!      open(unit=33,file='plane-res/sigma-analytic-50l.dat')
!      do i=1,npts
!        write(33,*) real(sigma(i)),imag(sigma(i))
!      enddo
!      close(33)


      call date_and_time(date,time,zone,ivals)
      fname='./sigma-'//date//'-'//time(1:6)//'.dat'
      open(unit=33,file=fname)
      do i=1,2*npts
        write(33,*) real(sigma(i)),imag(sigma(i))
      enddo
      close(33)

      fname='./pw-rhs.dat'
      open(unit=33,file=fname)
      do i=1,2*npts
        write(33,*) real(rhs(i)),imag(rhs(i))
      enddo

      allocate(rsigma(npts))
!$OMP PARALLEL DO DEFAULT(SHARED)      
      do i=1,npts
        rsigma(i) = abs(sigma(i))
      enddo
!$OMP END PARALLEL DO      

      fname = './fresnel-pw-absrho.vtk'
      title = 'rho abs'
      call surf_vtk_plot_scalar(npatches,norders,ixyzs,iptype,npts, &
        srccoefs,srcvals,rsigma,fname,title)

!		call test_accuracy_transmission(eps,sigma,zpars,npts,wts,srcvals,xyz_in,xyz_out)
        
       nlat = 1000
       ntarg = nlat*nlat

      allocate(targs(3,ntarg),ipatch_id(ntarg),uvs_targ(2,ntarg))
      xyz_start(1) = 0.0d0
      xyz_start(2) = -25.0d0
      xyz_start(3) = 4.0d0

      dxyz(1) = 1.0d0
      dxyz(2) = 50.0d0/(nlat+0.0d0)
      dxyz(3) = 46.0d0/(nlat+0.0d0)

      do i=1,nlat
        do j=1,nlat
          ipt = (i-1)*nlat+j
          targs(1,ipt) = xyz_start(1) 
          targs(2,ipt) = xyz_start(2) + dxyz(2)*i 
          targs(3,ipt) = xyz_start(3) + dxyz(3)*j
          ipatch_id(ipt) = -1
          uvs_targ(1,ipt) = 0.0d0
          uvs_targ(2,ipt) = 0.0d0
        enddo
      enddo

      allocate(pvals(ntarg))
      do i=1,ntarg
        pvals(i) = 0
      enddo

      zpars2(1) = zk0
      zpars2(2) = ep0**2
      zpars2(3) = ep0
      ndtarg = 3

      call cpu_time(t1)

!$  t1 = omp_get_wtime()      
      call lpcomp_helm_comb_split_dir(npatches,norders,ixyzs,iptype, &
       npts,srccoefs,srcvals,ndtarg,ntarg,targs,ipatch_id, &
       uvs_targ,eps,zpars2,sigma,pvals)
     
       call cpu_time(t2)
!$   t2 = omp_get_wtime() 
     call prin2('target potential time=*',t2-t1,1)
      allocate(evals(ntarg))
      do i=1,ntarg
        ztmp = exp(ima*zk0*targs(3,i))
        evals(i) = abs(pvals(i)+ztmp)
      enddo
      
      fname = './fresnel_pot_ext.vtk'
      title = 'abc'
      ndims(1) = 1
      ndims(2) = nlat
      ndims(3) = nlat
      call vtk_write_plane(ndims,ntarg,xyz_start,dxyz,evals,title,fname)


      stop
      end



      subroutine test_exterior_pt(npatches,norder,npts,srcvals,&
     &srccoefs,wts,xyzout,isout)
!
!
!  this subroutine tests whether the pt xyzin, is
!  in the exterior of a surface, and also estimates the error
!  in representing e^{ir/2}/r and \grad e^{ir/2}/r \cdot n
!  centered at the interior point. Whether a point 
!  is in the interior or not is tested using Gauss' 
!  identity for the flux due to a point charge
!
!
!  input:
!    npatches - integer
!       number of patches
!    norder - integer
!       order of discretization
!    npts - integer
!       total number of discretization points on the surface
!    srccoefs - real *8 (9,npts)
!       koornwinder expansion coefficients of geometry info
!    xyzout -  real *8 (3)
!       point to be tested
!
!  output: 
!    isout - boolean
!      whether the target is in the interior or not
!

      implicit none
      integer npatches,norder,npts,npols
      real *8 srccoefs(9,npts),srcvals(12,npts),xyzout(3),wts(npts)
      real *8 tmp(3)
      real *8 dpars,done,pi
      real *8, allocatable :: rsurf(:),err_p(:,:) 
      integer ipars,norderhead,nd
      complex *16, allocatable :: sigma_coefs(:,:), sigma_vals(:,:)
      complex *16 zk,val

      integer ipatch,j,i
      real *8 ra,ds
      logical isout

      done = 1
      pi = atan(done)*4

      npols = (norder+1)*(norder+2)/2


      zk = 0

      ra = 0



      do ipatch=1,npatches
        do j=1,npols
          i = (ipatch-1)*npols + j
          call h3d_sprime(xyzout,srcvals(1,i),dpars,zk,ipars,val)
          call cross_prod3d(srcvals(4,i),srcvals(7,i),tmp)
          ds = sqrt(tmp(1)**2 + tmp(2)**2 + tmp(3)**2)
          ra = ra + real(val)*wts(i)
        enddo
      enddo

      if(abs(ra+4*pi).le.1.0d-3) isout = .false.
      if(abs(ra).le.1.0d-3) isout = .true.

      return
      end
