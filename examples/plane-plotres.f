      implicit real *8 (a-h,o-z) 
      real *8, allocatable :: srcvals(:,:),srccoefs(:,:)
      real *8, allocatable :: wts(:)
      integer ipars(2)

      integer, allocatable :: norders(:),ixyzs(:),iptype(:)
      integer, allocatable :: row_ptr(:),col_ind(:)
      real *8, allocatable :: xyz_in(:,:),xyz_out(:,:)
      real *8, allocatable :: xyz_src(:,:)
      real *8, allocatable :: rads(:),cms(:,:),rad_near(:)
      real *8, allocatable :: targs(:,:)

      complex *16, allocatable :: sigma(:),rhs(:)
      complex *16, allocatable :: sigma_coefs(:)
      real *8, allocatable :: errp_dens(:,:),rsurf(:)
      real *8 errm_dens(2)
      real *8, allocatable :: rsigma(:)
      real *8, allocatable :: errs(:)
      real *8 thet,phi
      complex * 16 zpars(3)
      real *8 xyz_start(3),dxyz(3)
      integer ndims(3)
      real *8, allocatable :: evals(:),evals2(:),rpot(:)
      complex *16, allocatable :: pvals(:),pvalsex(:)
      
      integer numit,niter
      character *100 title,fname

      integer, allocatable :: ipatch_id(:)
      real *8, allocatable :: uvs_targ(:,:)

      logical isout0,isout1
      logical, allocatable :: isout(:)

      complex *16 pot,potex,ztmp,ima

      data ima/(0.0d0,1.0d0)/


      call prini(6,13)

      done = 1
      pi = atan(done)*4

c
c   simulation for plane 50 wavelengths in size
c
      

      fname = '../../fmm3dbie/geometries/A380_Final_o03_r02.go3'
      
      call open_gov3_geometry_mem(fname,npatches,npts)

      call prinf('npatches=*',npatches,1)
      call prinf('npts=*',npts,1)

      allocate(srcvals(12,npts),srccoefs(9,npts))
      allocate(ixyzs(npatches+1),iptype(npatches),norders(npatches))
      allocate(wts(npts))

      call open_gov3_geometry(fname,npatches,norders,ixyzs,
     1   iptype,npts,srcvals,srccoefs,wts)
      print *, "done reading geometry"

      allocate(sigma(npts),rhs(npts),rsigma(npts),sigma_coefs(npts))
      
      fname = 'plane-res/pottarg-20200424-091814.dat'
      nlat = 301
      ntarg = nlat*nlat

      allocate(evals(ntarg),evals2(ntarg),pvals(ntarg),pvalsex(ntarg))
      allocate(rpot(ntarg))
      open(unit=33,file=fname)
      do i=1,ntarg
        read(33,*) rtmp1,rtmp2,rtmp3,rtmp4,evals(i),evals2(i)
        pvals(i) = rtmp1+ima*rtmp2
        pvalsex(i) = rtmp3+ima*rtmp4
      enddo

      close(33)
      print *, "done reading pot"

      fname = 'plane-res/sigma-20200424-091814.dat'
      open(unit=33,file=fname)
      do i=1,npts
        read(33,*) rtmp1,rtmp2
        sigma(i) = rtmp1 + ima*rtmp2
      enddo
      close(33)
      fname = 'plane-res/rhs-20200424-091814.dat'
      open(unit=33,file=fname)
      do i=1,npts
        read(33,*) rtmp1,rtmp2
        rhs(i) = rtmp1 + ima*rtmp2
      enddo
      close(33)

      print *, "done reading rhs and sigma"
      call prin2('rhs=*',rhs,24)
      call prin2('sigma=*',sigma,24)
c
c       write out vtk files
c

C$OMP PARALLEL DO DEFAULT(SHARED)      
      do i=1,npts
        rsigma(i) = real(sigma(i))
      enddo
C$OMP END PARALLEL DO      

      call surf_vals_to_coefs(2,npatches,norders,ixyzs,iptype,npts,
     1   sigma,sigma_coefs)
      
      allocate(errp_dens(2,npatches),rsurf(npatches))
      do i=1,npatches
        rsurf(i) = 0
        do j=ixyzs(i),ixyzs(i+1)-1
          rsurf(i) = rsurf(i) + wts(j)
        enddo
      enddo

      call prin2('rsurf=*',rsurf,24)

      call surf_fun_error(2,npatches,norders,ixyzs,iptype,npts,
     1   rsurf,sigma_coefs,errp_dens,errm_dens)
      call prin2('errp_dens=*',errp_dens,24)

      fname = 'plane-res/a380_rsigma.vtk'
      title = 'a380 - real part'
      call surf_vtk_plot_scalar(npatches,norders,ixyzs,iptype,npts,
     1   srccoefs,srcvals,rsigma,fname,title)
       print *, "done plotting sigma"

C$OMP PARALLEL DO DEFAULT(SHARED)      
      do i=1,npts
        rsigma(i) = real(rhs(i))
      enddo
C$OMP END PARALLEL DO      

      fname = 'plane-res/a380_rrhs.vtk'
      title = 'a380 - real part'
      call surf_vtk_plot_scalar(npatches,norders,ixyzs,iptype,npts,
     1   srccoefs,srcvals,rsigma,fname,title)
      print *, "done plotting rhs"

      do i=1,npatches
        rmax = max(errp_dens(1,i),errp_dens(2,i))
        do j=ixyzs(i),ixyzs(i+1)-1
          rsigma(j) = log(rmax)/log(10.0d0)
        enddo
      enddo
      fname = 'plane-res/a380_rsigmaerr.vtk'
      title = 'a380 - real part'
      call surf_vtk_plot_scalar(npatches,norders,ixyzs,iptype,npts,
     1   srccoefs,srcvals,rsigma,fname,title)
      print *, "done plotting rsigma"

      xyz_start(1) = -0.5d0
      xyz_start(2) = -6.0d0
      xyz_start(3) = -12.0d0

      dxyz(1) = 0.04d0
      dxyz(2) = 0.04d0
      dxyz(3) = 1.0d0

      ndims(1) = nlat
      ndims(2) = nlat
      ndims(3) = 1
      fname = 'plane-res/a380_err.vtk'
      title = 'abc'
      call vtk_write_plane(ndims,ntarg,xyz_start,dxyz,evals,title,
     1   fname)
      print *, "done plotting err"

      fname = 'plane-res/a380_errrel.vtk'
      title = 'abc'
      xyz_start(3) = -2
      call vtk_write_plane(ndims,ntarg,xyz_start,dxyz,evals2,title,
     1   fname)
      print *, "done plotting errrel"

      
      

      stop
      end
c
c
c
c
c
