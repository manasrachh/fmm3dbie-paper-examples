      implicit real *8 (a-h,o-z) 
      real *8, allocatable :: srcvals(:,:),srccoefs(:,:)
      real *8, allocatable :: wts(:),rsigma(:)
      integer ipars(2)

      integer, allocatable :: norders(:),ixyzs(:),iptype(:)

      real *8 xyz_out(3),xyz_in(3)
      complex *16, allocatable :: sigma(:),rhs(:)
      complex *16, allocatable :: sigma2(:),rhs2(:)
      real *8, allocatable :: errs(:)
      real *8 thet,phi,eps_gmres
      complex * 16 zpars(3)
      integer numit,niter
      character *100 title,fname

      integer ipatch_id
      real *8 uvs_targ(2)

      logical isout0,isout1

      complex *16 pot,potex,ztmp,ima

      data ima/(0.0d0,1.0d0)/


      call prini(6,13)

      done = 1
      pi = atan(done)*4

c
c   simulation for plane 50 wavelengths in size
c
      zk = 15.7d0*2
      zk = 0.1d0
      zpars(1) = zk 
      zpars(2) = -ima*zk
      zpars(3) = 2.0d0

      
      xyz_in(1) = 2.01d0
      xyz_in(2) = 0.0d-5
      xyz_in(3) = 0.5d0

      xyz_out(1) = -3.5d0
      xyz_out(2) = 3.1d0
      xyz_out(3) = 20.1d0

      fname = '../../fmm3dbie/geometries/A380_Final_o03_r00.go3'
      
      call open_gov3_geometry_mem(fname,npatches,npts)

      call prinf('npatches=*',npatches,1)
      call prinf('npts=*',npts,1)

      allocate(srcvals(12,npts),srccoefs(9,npts))
      allocate(ixyzs(npatches+1),iptype(npatches),norders(npatches))
      allocate(wts(npts))

      call open_gov3_geometry(fname,npatches,norders,ixyzs,
     1   iptype,npts,srcvals,srccoefs,wts)
      
cc      fname = 'plane-res/a380.vtk'
cc      title = 'a380'
cc      call surf_vtk_plot(npatches,norders,ixyzs,iptype,npts,
cc     1   srccoefs,srcvals,fname,title)

      norder = norders(1)
      call test_exterior_pt(npatches,norder,npts,srcvals,srccoefs,
     1  wts,xyz_in,isout0)
      
      call test_exterior_pt(npatches,norder,npts,srcvals,srccoefs,
     1   wts,xyz_out,isout1)

       print *, isout0,isout1


      allocate(sigma(npts),rhs(npts))
      allocate(sigma2(npts),rhs2(npts))
      ifinout = 1

      thet = hkrand(0)*pi
      phi = hkrand(0)*2*pi
      do i=1,npts
        if(ifinout.eq.0) 
     1     call h3d_slp(xyz_out,3,srcvals(1,i),0,dpars,1,zpars,0,
     2      ipars,rhs(i))
        if(ifinout.eq.1) 
     1     call h3d_slp(xyz_in,3,srcvals(1,i),0,dpars,1,zpars,0,
     2      ipars,rhs(i))
        rhs(i) = rhs(i)
        x = srcvals(1,i)*cos(thet)
        y = srcvals(2,i)*sin(thet)*cos(phi)
        z = srcvals(3,i)*sin(thet)*sin(phi)
        rhs2(i) = exp(ima*zk*(x+y+z))
        sigma(i) = 0
        sigma2(i) = 0
      enddo


      numit = 400
      niter = 0
      allocate(errs(numit+1))

      eps = 0.51d-6
      eps_gmres = eps

      call cpu_time(t1)
C$      t1 = omp_get_wtime()      
      call helm_comb_dir_solver(npatches,norders,ixyzs,iptype,npts,
     1  srccoefs,srcvals,eps,zpars,numit,ifinout,rhs,eps_gmres,
     2  niter,errs,rres,sigma)

      call prinf('niter=*',niter,1)
      call prin2('rres=*',rres,1)
      call prin2('errs=*',errs,niter)

      call cpu_time(t2)
C$       t2 = omp_get_wtime()
      call prin2('analytic solve time=*',t2-t1,1)

      open(unit=33,file='plane-res/sigma-analytic-50l.dat')
      do i=1,npts
        write(33,*) real(sigma(i)),imag(sigma(i))
      enddo
      close(33)


c
c       test solution at interior point
c
      call h3d_slp(xyz_out,3,xyz_in,0,dpars,1,zpars,0,ipars,potex)
      pot = 0
      do i=1,npts
        if(ifinout.eq.0) 
     1     call h3d_comb(srcvals(1,i),3,xyz_in,0,dpars,3,zpars,0,
     2      ipars,ztmp)
        if(ifinout.eq.1) 
     1     call h3d_comb(srcvals(1,i),3,xyz_out,0,dpars,3,zpars,0,
     2      ipars,ztmp)
        pot = pot + sigma(i)*wts(i)*ztmp
      enddo

      call prin2('potex=*',potex,2)
      call prin2('pot=*',pot,2)
      erra = abs(pot-potex)/abs(potex)
      call prin2('relative error=*',erra,1)

      allocate(rsigma(npts))
      do i=1,npts
        rsigma(i) = real(sigma(i))*100
      enddo

      fname = 'plane-res/a380_rsigma_50l_ext.vtk'
      title = 'a380 - real part'
      call surf_vtk_plot_scalar(npatches,norders,ixyzs,iptype,npts,
     1   srccoefs,srcvals,rsigma,fname,title)

      call cpu_time(t1)
C$      t1 = omp_get_wtime()      
      call helm_comb_dir_solver(npatches,norders,ixyzs,iptype,npts,
     1  srccoefs,srcvals,eps,zpars,numit,ifinout,rhs2,eps_gmres,
     2  niter,errs,rres,sigma2)
      call cpu_time(t2)
C$      t2 = omp_get_wtime()      

      call prin2('pw solve time=*',t2-t1,1)
      

      open(unit=33,file='plane-res/sigma-pw-50l.dat')
      do i=1,npts
        write(33,*) real(sigma2(i)),imag(sigma2(i))
      enddo
      close(33)

      do i=1,npts
        rsigma(i) = real(sigma2(i))*100
      enddo

      fname = 'plane-res/a380_rsigma2_50l_ext.vtk'
      title = 'a380 - real part - pw data'
      call surf_vtk_plot_scalar(npatches,norders,ixyzs,iptype,npts,
     1   srccoefs,srcvals,rsigma,fname,title)

      stop
      end




      subroutine test_exterior_pt(npatches,norder,npts,srcvals,
     1   srccoefs,wts,xyzout,isout)
c
c
c  this subroutine tests whether the pt xyzin, is
c  in the exterior of a surface, and also estimates the error
c  in representing e^{ir/2}/r and \grad e^{ir/2}/r \cdot n
c  centered at the interior point. Whether a point 
c  is in the interior or not is tested using Gauss' 
c  identity for the flux due to a point charge
c
c
c  input:
c    npatches - integer
c       number of patches
c    norder - integer
c       order of discretization
c    npts - integer
c       total number of discretization points on the surface
c    srccoefs - real *8 (9,npts)
c       koornwinder expansion coefficients of geometry info
c    xyzout -  real *8 (3)
c       point to be tested
c
c  output: 
c    isout - boolean
c      whether the target is in the interior or not
c

      implicit none
      integer npatches,norder,npts,npols
      real *8 srccoefs(9,npts),srcvals(12,npts),xyzout(3),wts(npts)
      real *8 tmp(3)
      real *8 dpars,done,pi
      real *8, allocatable :: rsurf(:),err_p(:,:) 
      integer ipars,norderhead,nd
      complex *16, allocatable :: sigma_coefs(:,:), sigma_vals(:,:)
      complex *16 zk,val

      integer ipatch,j,i
      real *8 ra,ds
      logical isout

      done = 1
      pi = atan(done)*4

      npols = (norder+1)*(norder+2)/2


      zk = 0

      ra = 0



      do ipatch=1,npatches
        do j=1,npols
          i = (ipatch-1)*npols + j
          call h3d_sprime(xyzout,12,srcvals(1,i),0,dpars,1,zk,0,
     1       ipars,val)
          call cross_prod3d(srcvals(4,i),srcvals(7,i),tmp)
          ds = sqrt(tmp(1)**2 + tmp(2)**2 + tmp(3)**2)
          ra = ra + real(val)*wts(i)
        enddo
      enddo

      if(abs(ra+4*pi).le.1.0d-3) isout = .false.
      if(abs(ra).le.1.0d-3) isout = .true.

      return
      end

   




